![](http://osx.hyperjeff.net/images/auto3.png)
# Safari Tabs

This is an AppleScript wrapped in a System Service
  which scans all Safari tabs and creates a single HTML file with all the names linked with their URLs,
  grouped by Safari window.

Once run, you can now safely close all your browser tabs and use this file to refer to the pages as needed.

Bonus service: There is also a service to count the number of tabs you have open to gauge your situation.

Double bonus: You can restore any previous set of windows and tabs with the Restore app.

It is the goal of these items to work non-destructively.
Saving out windows and tabs won't close any tabs or windows on you.
Restoring windows and tabs just adds to what's already open.

## Installing

### Install Workflow Apps

Double-click the .workflow files, or manually copy them to the ~/Library/Services folder.

Note: Double-clicking a workflow file will move it to the Services folder, so if you want to still be able to edit it and commit changes, etc,
then I'd advise manually copying the .workflow files to the Services folder.

### Install Restore App

To restore the collection of windows and tabs save from the "Tabs to HTML" service,
just drag-n-drop the .html file onto this app.

## Editing

The easiest way to edit or extract the AppleScript source is to open the .workflow file in Automator.
Don't directly edit the document.wflow file.
Or do it. I'm not your mom.

To edit the Restore app, drag-n-drop the app itself onto Automator.

## Safari Optimization Note

Safari has an "optimization" that goes as follows:
If you start Safari and it restores windows from your last session,
it doesn't automatically load tabs that are not foremost in a window.
If Safari doesn't load a tab and you ask it for its URL via AppleScript,
it will return "missing value" and the HTML file it produces will be filled with broken links.

Not to fear, the Tabs to HTML script can detect this and trigger Safari to load all tabs before asking for their URLs.
Since this can take a fair amount of time, depending on how many tabs are open,
a system notification is sent trying to tell the user as much.

Ideally I'd like to have a proper progress bar shown, but since this is run as a system service it's not possible.
Still the little progress wheel for the script that shows up when you click on the script widget in the menubar
should show progress, but alas that also does not seem to be working.
Perhaps someone (read: *you*) can fix this bug.

## Other things to be done with it

Modify the source to work with Chrome, FireFox, OmniWeb, etc.

Separate out the AppleScript and run it periodically to produce chronological snapshots of your browsing life.

Use the "Tabs to HTML" and "Restore" together to export and restore working sets.

(The great OmniWeb browser has this idea of workspaces, which no other browsers have taken up,
so these scripts at least go some ways toward making this available.)